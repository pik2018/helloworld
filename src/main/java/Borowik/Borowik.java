package Borowik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Borowik {

    public static void main(String[] args) {
        SpringApplication.run(Borowik.class, args);
    }

}